
var request = require('request');
var async = require('async');

var e = {};
e.url = null;

e.getESRequestOpt = function (url, dataObj) {
    var data = JSON.stringify(dataObj);
    return {
        headers: {
            'content-type': 'text/plain;charset=utf-8',
            'content-length': Buffer.byteLength(data, 'utf8')
        },
        url: url,
        body: data
    };
};

e.removeIndex = function(cb) {
	request.del(e.url, function (error, response, body) {
		console.log(body);
		cb();
	});
};

e.createIndex = function(cb) {
	request.post(e.url, function (error, response, body) {
		console.log(body);
		cb();
	});
};

e.addDocs = function(type, docs, cb) {
	var tasks = [];
	for (var i = 0; i < docs.length; i++) {
		var doc = docs[i];
		tasks.push(function (doc) {
			return function (cb) {
				var currUrl = e.url + "/"+type+"/" + doc._id;
				request.post(e.getESRequestOpt(currUrl, doc), function (err, res, body) {
					if (err) {
						cb(null, err);
					} else {
						cb(null, body);
					}
				});
			};
		}(doc));
	}
	async.parallel(tasks, function (err, results) {
		cb(results);
	});
};

e.addDoc = function(type, doc, cb) {
	var currUrl = e.url + "/"+type+"/" + doc._id;
	request.post(e.getESRequestOpt(currUrl, doc), function (err, res, body) {
		cb(err, res, body);
	});
};

module.exports = e;
